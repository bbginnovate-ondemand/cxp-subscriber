CXP Subscriber Drupal 7 Module
=============================================================================================

A Drupal module for automated subscriber communication with the BBG Content Exchange Platform.
See: https://oddi.bbg.gov/cxp/#/docs for documentation on the CXP API.

This module would be used on an Affiliate website to access the CXP-API web service
and retrieve and publish content within the Affiliate's Drupal website, should the affiliate
wish to do so.

The Affiliate must contact the CXP-API administrators to obtain a API Key. This API Key
is set in drupal's site as detailed below:

   setting.php file config variable:
   
   cxp_subscriber_api_key = <api key from oddi cxp api>

 for example in the <drupalroot>/sites/default/settings.php file:
   $conf['cxp_subscriber_api_key'] = 'yhb1Xgr9p9G0eEE1YTPj8usTUc33MbRrhtUppnNl';

 The API Key variable setting may be revieed after enabling the module within drupal at:
   http://localhost/drupal/admin/config/system/variable/module

Prior to module enablement:
   install contrib modules:
       bundle_copy
       field_group
       Taxonomy_xml

   enable Taxonomy in core modules.
          Image Module in core modules.

   import content_type: audio_video
   import taxonomy vocabularies using Taxonomy_XML module:
              Category
              Genre 
              Media Type
              Network
              Programs
              Region
              Tags

============================  audio_video content type definition ===  use with bundle_copy module

$data = array(
  'bundles' => array(
    'audio_video' => (object) array(
      'type' => 'audio_video',
      'name' => 'Audio/Video',
      'base' => 'node_content',
      'module' => 'node',
      'description' => 'Audio and Video content',
      'help' => '',
      'has_title' => '1',
      'title_label' => 'Title',
      'custom' => '1',
      'modified' => '1',
      'locked' => '0',
      'disabled' => '0',
      'orig_type' => 'audio_video',
      'disabled_changed' => FALSE,
      'bc_entity_type' => 'node',
    ),
  ),
  'fields' => array(
    'body' => array(
      'entity_types' => array(
        0 => 'node',
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'settings' => array(),
      'translatable' => '0',
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_body' => array(
                'value' => 'body_value',
                'summary' => 'body_summary',
                'format' => 'body_format',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_body' => array(
                'value' => 'body_value',
                'summary' => 'body_summary',
                'format' => 'body_format',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'format' => array(
          'table' => 'filter_format',
          'columns' => array(
            'format' => 'format',
          ),
        ),
      ),
      'field_name' => 'body',
      'type' => 'text_with_summary',
      'module' => 'text',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'columns' => array(
        'value' => array(
          'type' => 'text',
          'size' => 'big',
          'not null' => FALSE,
        ),
        'summary' => array(
          'type' => 'text',
          'size' => 'big',
          'not null' => FALSE,
        ),
        'format' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'webform',
          1 => 'audio_video',
        ),
      ),
    ),
    'field_audio_transcript_media_bit' => array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'max_length' => '255',
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_audio_transcript_media_bit' => array(
                'value' => 'field_audio_transcript_media_bit_value',
                'format' => 'field_audio_transcript_media_bit_format',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_audio_transcript_media_bit' => array(
                'value' => 'field_audio_transcript_media_bit_value',
                'format' => 'field_audio_transcript_media_bit_format',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'format' => array(
          'table' => 'filter_format',
          'columns' => array(
            'format' => 'format',
          ),
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'field_name' => 'field_audio_transcript_media_bit',
      'type' => 'text',
      'module' => 'text',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'columns' => array(
        'value' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => FALSE,
        ),
        'format' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'audio_video',
        ),
      ),
    ),
    'field_audio_transcript_media_fil' => array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'max_length' => '255',
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_audio_transcript_media_fil' => array(
                'value' => 'field_audio_transcript_media_fil_value',
                'format' => 'field_audio_transcript_media_fil_format',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_audio_transcript_media_fil' => array(
                'value' => 'field_audio_transcript_media_fil_value',
                'format' => 'field_audio_transcript_media_fil_format',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'format' => array(
          'table' => 'filter_format',
          'columns' => array(
            'format' => 'format',
          ),
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'field_name' => 'field_audio_transcript_media_fil',
      'type' => 'text',
      'module' => 'text',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'columns' => array(
        'value' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => FALSE,
        ),
        'format' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'audio_video',
        ),
      ),
    ),
    'field_audio_transcript_media_url' => array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'max_length' => '255',
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_audio_transcript_media_url' => array(
                'value' => 'field_audio_transcript_media_url_value',
                'format' => 'field_audio_transcript_media_url_format',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_audio_transcript_media_url' => array(
                'value' => 'field_audio_transcript_media_url_value',
                'format' => 'field_audio_transcript_media_url_format',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'format' => array(
          'table' => 'filter_format',
          'columns' => array(
            'format' => 'format',
          ),
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'field_name' => 'field_audio_transcript_media_url',
      'type' => 'text',
      'module' => 'text',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'columns' => array(
        'value' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => FALSE,
        ),
        'format' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'audio_video',
        ),
      ),
    ),
    'field_category' => array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'allowed_values' => array(
          0 => array(
            'vocabulary' => 'category',
            'parent' => '0',
          ),
        ),
        'options_list_callback' => 'i18n_taxonomy_allowed_values',
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_category' => array(
                'tid' => 'field_category_tid',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_category' => array(
                'tid' => 'field_category_tid',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'tid' => array(
          'table' => 'taxonomy_term_data',
          'columns' => array(
            'tid' => 'tid',
          ),
        ),
      ),
      'indexes' => array(
        'tid' => array(
          0 => 'tid',
        ),
      ),
      'field_name' => 'field_category',
      'type' => 'taxonomy_term_reference',
      'module' => 'taxonomy',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '-1',
      'deleted' => '0',
      'columns' => array(
        'tid' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => FALSE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'audio_video',
        ),
      ),
    ),
    'field_credit' => array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'max_length' => '255',
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_credit' => array(
                'value' => 'field_credit_value',
                'format' => 'field_credit_format',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_credit' => array(
                'value' => 'field_credit_value',
                'format' => 'field_credit_format',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'format' => array(
          'table' => 'filter_format',
          'columns' => array(
            'format' => 'format',
          ),
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'field_name' => 'field_credit',
      'type' => 'text',
      'module' => 'text',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'columns' => array(
        'value' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => FALSE,
        ),
        'format' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'audio_video',
        ),
      ),
    ),
    'field_cxp_id' => array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'max_length' => '255',
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_cxp_id' => array(
                'value' => 'field_cxp_id_value',
                'format' => 'field_cxp_id_format',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_cxp_id' => array(
                'value' => 'field_cxp_id_value',
                'format' => 'field_cxp_id_format',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'format' => array(
          'table' => 'filter_format',
          'columns' => array(
            'format' => 'format',
          ),
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'field_name' => 'field_cxp_id',
      'type' => 'text',
      'module' => 'text',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'columns' => array(
        'value' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => FALSE,
        ),
        'format' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'audio_video',
        ),
      ),
    ),
    'field_display_date' => array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'max_length' => '255',
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_display_date' => array(
                'value' => 'field_display_date_value',
                'format' => 'field_display_date_format',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_display_date' => array(
                'value' => 'field_display_date_value',
                'format' => 'field_display_date_format',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'format' => array(
          'table' => 'filter_format',
          'columns' => array(
            'format' => 'format',
          ),
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'field_name' => 'field_display_date',
      'type' => 'text',
      'module' => 'text',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'columns' => array(
        'value' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => FALSE,
        ),
        'format' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'audio_video',
        ),
      ),
    ),
    'field_high_quality_media_bitrate' => array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'max_length' => '255',
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_high_quality_media_bitrate' => array(
                'value' => 'field_high_quality_media_bitrate_value',
                'format' => 'field_high_quality_media_bitrate_format',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_high_quality_media_bitrate' => array(
                'value' => 'field_high_quality_media_bitrate_value',
                'format' => 'field_high_quality_media_bitrate_format',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'format' => array(
          'table' => 'filter_format',
          'columns' => array(
            'format' => 'format',
          ),
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'field_name' => 'field_high_quality_media_bitrate',
      'type' => 'text',
      'module' => 'text',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'columns' => array(
        'value' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => FALSE,
        ),
        'format' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'audio_video',
        ),
      ),
    ),
    'field_high_quality_media_filesiz' => array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'max_length' => '255',
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_high_quality_media_filesiz' => array(
                'value' => 'field_high_quality_media_filesiz_value',
                'format' => 'field_high_quality_media_filesiz_format',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_high_quality_media_filesiz' => array(
                'value' => 'field_high_quality_media_filesiz_value',
                'format' => 'field_high_quality_media_filesiz_format',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'format' => array(
          'table' => 'filter_format',
          'columns' => array(
            'format' => 'format',
          ),
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'field_name' => 'field_high_quality_media_filesiz',
      'type' => 'text',
      'module' => 'text',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'columns' => array(
        'value' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => FALSE,
        ),
        'format' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'audio_video',
        ),
      ),
    ),
    'field_high_quality_media_height' => array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'max_length' => '128',
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_high_quality_media_height' => array(
                'value' => 'field_high_quality_media_height_value',
                'format' => 'field_high_quality_media_height_format',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_high_quality_media_height' => array(
                'value' => 'field_high_quality_media_height_value',
                'format' => 'field_high_quality_media_height_format',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'format' => array(
          'table' => 'filter_format',
          'columns' => array(
            'format' => 'format',
          ),
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'field_name' => 'field_high_quality_media_height',
      'type' => 'text',
      'module' => 'text',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'columns' => array(
        'value' => array(
          'type' => 'varchar',
          'length' => '128',
          'not null' => FALSE,
        ),
        'format' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'audio_video',
        ),
      ),
    ),
    'field_high_quality_media_url' => array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'max_length' => '255',
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_high_quality_media_url' => array(
                'value' => 'field_high_quality_media_url_value',
                'format' => 'field_high_quality_media_url_format',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_high_quality_media_url' => array(
                'value' => 'field_high_quality_media_url_value',
                'format' => 'field_high_quality_media_url_format',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'format' => array(
          'table' => 'filter_format',
          'columns' => array(
            'format' => 'format',
          ),
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'field_name' => 'field_high_quality_media_url',
      'type' => 'text',
      'module' => 'text',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'columns' => array(
        'value' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => FALSE,
        ),
        'format' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'audio_video',
        ),
      ),
    ),
    'field_high_quality_media_width' => array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'max_length' => '128',
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_high_quality_media_width' => array(
                'value' => 'field_high_quality_media_width_value',
                'format' => 'field_high_quality_media_width_format',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_high_quality_media_width' => array(
                'value' => 'field_high_quality_media_width_value',
                'format' => 'field_high_quality_media_width_format',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'format' => array(
          'table' => 'filter_format',
          'columns' => array(
            'format' => 'format',
          ),
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'field_name' => 'field_high_quality_media_width',
      'type' => 'text',
      'module' => 'text',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'columns' => array(
        'value' => array(
          'type' => 'varchar',
          'length' => '128',
          'not null' => FALSE,
        ),
        'format' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'audio_video',
        ),
      ),
    ),
    'field_image_height' => array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'max_length' => '255',
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_image_height' => array(
                'value' => 'field_image_height_value',
                'format' => 'field_image_height_format',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_image_height' => array(
                'value' => 'field_image_height_value',
                'format' => 'field_image_height_format',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'format' => array(
          'table' => 'filter_format',
          'columns' => array(
            'format' => 'format',
          ),
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'field_name' => 'field_image_height',
      'type' => 'text',
      'module' => 'text',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'columns' => array(
        'value' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => FALSE,
        ),
        'format' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'audio_video',
        ),
      ),
    ),
    'field_image_url' => array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'max_length' => '255',
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_image_url' => array(
                'value' => 'field_image_url_value',
                'format' => 'field_image_url_format',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_image_url' => array(
                'value' => 'field_image_url_value',
                'format' => 'field_image_url_format',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'format' => array(
          'table' => 'filter_format',
          'columns' => array(
            'format' => 'format',
          ),
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'field_name' => 'field_image_url',
      'type' => 'text',
      'module' => 'text',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'columns' => array(
        'value' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => FALSE,
        ),
        'format' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'audio_video',
        ),
      ),
    ),
    'field_image_width' => array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'max_length' => '255',
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_image_width' => array(
                'value' => 'field_image_width_value',
                'format' => 'field_image_width_format',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_image_width' => array(
                'value' => 'field_image_width_value',
                'format' => 'field_image_width_format',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'format' => array(
          'table' => 'filter_format',
          'columns' => array(
            'format' => 'format',
          ),
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'field_name' => 'field_image_width',
      'type' => 'text',
      'module' => 'text',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'columns' => array(
        'value' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => FALSE,
        ),
        'format' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'audio_video',
        ),
      ),
    ),
    'field_import_id' => array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'max_length' => '255',
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_import_id' => array(
                'value' => 'field_import_id_value',
                'format' => 'field_import_id_format',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_import_id' => array(
                'value' => 'field_import_id_value',
                'format' => 'field_import_id_format',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'format' => array(
          'table' => 'filter_format',
          'columns' => array(
            'format' => 'format',
          ),
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'field_name' => 'field_import_id',
      'type' => 'text',
      'module' => 'text',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'columns' => array(
        'value' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => FALSE,
        ),
        'format' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'audio_video',
        ),
      ),
    ),
    'field_import_source' => array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'max_length' => '255',
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_import_source' => array(
                'value' => 'field_import_source_value',
                'format' => 'field_import_source_format',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_import_source' => array(
                'value' => 'field_import_source_value',
                'format' => 'field_import_source_format',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'format' => array(
          'table' => 'filter_format',
          'columns' => array(
            'format' => 'format',
          ),
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'field_name' => 'field_import_source',
      'type' => 'text',
      'module' => 'text',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'columns' => array(
        'value' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => FALSE,
        ),
        'format' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'audio_video',
        ),
      ),
    ),
    'field_media_duration' => array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'max_length' => '255',
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_media_duration' => array(
                'value' => 'field_media_duration_value',
                'format' => 'field_media_duration_format',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_media_duration' => array(
                'value' => 'field_media_duration_value',
                'format' => 'field_media_duration_format',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'format' => array(
          'table' => 'filter_format',
          'columns' => array(
            'format' => 'format',
          ),
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'field_name' => 'field_media_duration',
      'type' => 'text',
      'module' => 'text',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'columns' => array(
        'value' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => FALSE,
        ),
        'format' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'audio_video',
        ),
      ),
    ),
    'field_mobile_media_bitrate' => array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'max_length' => '255',
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_mobile_media_bitrate' => array(
                'value' => 'field_mobile_media_bitrate_value',
                'format' => 'field_mobile_media_bitrate_format',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_mobile_media_bitrate' => array(
                'value' => 'field_mobile_media_bitrate_value',
                'format' => 'field_mobile_media_bitrate_format',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'format' => array(
          'table' => 'filter_format',
          'columns' => array(
            'format' => 'format',
          ),
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'field_name' => 'field_mobile_media_bitrate',
      'type' => 'text',
      'module' => 'text',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'columns' => array(
        'value' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => FALSE,
        ),
        'format' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'audio_video',
        ),
      ),
    ),
    'field_mobile_media_filesize' => array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'max_length' => '255',
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_mobile_media_filesize' => array(
                'value' => 'field_mobile_media_filesize_value',
                'format' => 'field_mobile_media_filesize_format',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_mobile_media_filesize' => array(
                'value' => 'field_mobile_media_filesize_value',
                'format' => 'field_mobile_media_filesize_format',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'format' => array(
          'table' => 'filter_format',
          'columns' => array(
            'format' => 'format',
          ),
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'field_name' => 'field_mobile_media_filesize',
      'type' => 'text',
      'module' => 'text',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'columns' => array(
        'value' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => FALSE,
        ),
        'format' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'audio_video',
        ),
      ),
    ),
    'field_mobile_media_height' => array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'max_length' => '128',
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_mobile_media_height' => array(
                'value' => 'field_mobile_media_height_value',
                'format' => 'field_mobile_media_height_format',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_mobile_media_height' => array(
                'value' => 'field_mobile_media_height_value',
                'format' => 'field_mobile_media_height_format',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'format' => array(
          'table' => 'filter_format',
          'columns' => array(
            'format' => 'format',
          ),
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'field_name' => 'field_mobile_media_height',
      'type' => 'text',
      'module' => 'text',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'columns' => array(
        'value' => array(
          'type' => 'varchar',
          'length' => '128',
          'not null' => FALSE,
        ),
        'format' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'audio_video',
        ),
      ),
    ),
    'field_mobile_media_url' => array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'max_length' => '255',
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_mobile_media_url' => array(
                'value' => 'field_mobile_media_url_value',
                'format' => 'field_mobile_media_url_format',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_mobile_media_url' => array(
                'value' => 'field_mobile_media_url_value',
                'format' => 'field_mobile_media_url_format',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'format' => array(
          'table' => 'filter_format',
          'columns' => array(
            'format' => 'format',
          ),
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'field_name' => 'field_mobile_media_url',
      'type' => 'text',
      'module' => 'text',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'columns' => array(
        'value' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => FALSE,
        ),
        'format' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'audio_video',
        ),
      ),
    ),
    'field_mobile_media_width' => array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'max_length' => '128',
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_mobile_media_width' => array(
                'value' => 'field_mobile_media_width_value',
                'format' => 'field_mobile_media_width_format',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_mobile_media_width' => array(
                'value' => 'field_mobile_media_width_value',
                'format' => 'field_mobile_media_width_format',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'format' => array(
          'table' => 'filter_format',
          'columns' => array(
            'format' => 'format',
          ),
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'field_name' => 'field_mobile_media_width',
      'type' => 'text',
      'module' => 'text',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'columns' => array(
        'value' => array(
          'type' => 'varchar',
          'length' => '128',
          'not null' => FALSE,
        ),
        'format' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'audio_video',
        ),
      ),
    ),
    'field_network' => array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'allowed_values' => array(
          0 => array(
            'vocabulary' => 'network',
            'parent' => '0',
          ),
        ),
        'options_list_callback' => 'i18n_taxonomy_allowed_values',
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_network' => array(
                'tid' => 'field_network_tid',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_network' => array(
                'tid' => 'field_network_tid',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'tid' => array(
          'table' => 'taxonomy_term_data',
          'columns' => array(
            'tid' => 'tid',
          ),
        ),
      ),
      'indexes' => array(
        'tid' => array(
          0 => 'tid',
        ),
      ),
      'field_name' => 'field_network',
      'type' => 'taxonomy_term_reference',
      'module' => 'taxonomy',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'columns' => array(
        'tid' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => FALSE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'audio_video',
        ),
      ),
    ),
    'field_programs' => array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'allowed_values' => array(
          0 => array(
            'vocabulary' => 'programs',
            'parent' => '0',
          ),
        ),
        'options_list_callback' => 'i18n_taxonomy_allowed_values',
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_programs' => array(
                'tid' => 'field_programs_tid',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_programs' => array(
                'tid' => 'field_programs_tid',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'tid' => array(
          'table' => 'taxonomy_term_data',
          'columns' => array(
            'tid' => 'tid',
          ),
        ),
      ),
      'indexes' => array(
        'tid' => array(
          0 => 'tid',
        ),
      ),
      'field_name' => 'field_programs',
      'type' => 'taxonomy_term_reference',
      'module' => 'taxonomy',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '2',
      'deleted' => '0',
      'columns' => array(
        'tid' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => FALSE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'audio_video',
        ),
      ),
    ),
    'field_region' => array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'allowed_values' => array(
          0 => array(
            'vocabulary' => 'region',
            'parent' => '0',
          ),
        ),
        'options_list_callback' => 'i18n_taxonomy_allowed_values',
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_region' => array(
                'tid' => 'field_region_tid',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_region' => array(
                'tid' => 'field_region_tid',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'tid' => array(
          'table' => 'taxonomy_term_data',
          'columns' => array(
            'tid' => 'tid',
          ),
        ),
      ),
      'indexes' => array(
        'tid' => array(
          0 => 'tid',
        ),
      ),
      'field_name' => 'field_region',
      'type' => 'taxonomy_term_reference',
      'module' => 'taxonomy',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'columns' => array(
        'tid' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => FALSE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'audio_video',
        ),
      ),
    ),
    'field_source_media_bitrate' => array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'max_length' => '255',
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_source_media_bitrate' => array(
                'value' => 'field_source_media_bitrate_value',
                'format' => 'field_source_media_bitrate_format',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_source_media_bitrate' => array(
                'value' => 'field_source_media_bitrate_value',
                'format' => 'field_source_media_bitrate_format',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'format' => array(
          'table' => 'filter_format',
          'columns' => array(
            'format' => 'format',
          ),
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'field_name' => 'field_source_media_bitrate',
      'type' => 'text',
      'module' => 'text',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'columns' => array(
        'value' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => FALSE,
        ),
        'format' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'audio_video',
        ),
      ),
    ),
    'field_source_media_filesize' => array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'max_length' => '255',
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_source_media_filesize' => array(
                'value' => 'field_source_media_filesize_value',
                'format' => 'field_source_media_filesize_format',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_source_media_filesize' => array(
                'value' => 'field_source_media_filesize_value',
                'format' => 'field_source_media_filesize_format',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'format' => array(
          'table' => 'filter_format',
          'columns' => array(
            'format' => 'format',
          ),
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'field_name' => 'field_source_media_filesize',
      'type' => 'text',
      'module' => 'text',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'columns' => array(
        'value' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => FALSE,
        ),
        'format' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'audio_video',
        ),
      ),
    ),
    'field_source_media_height' => array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'max_length' => '128',
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_source_media_height' => array(
                'value' => 'field_source_media_height_value',
                'format' => 'field_source_media_height_format',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_source_media_height' => array(
                'value' => 'field_source_media_height_value',
                'format' => 'field_source_media_height_format',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'format' => array(
          'table' => 'filter_format',
          'columns' => array(
            'format' => 'format',
          ),
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'field_name' => 'field_source_media_height',
      'type' => 'text',
      'module' => 'text',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'columns' => array(
        'value' => array(
          'type' => 'varchar',
          'length' => '128',
          'not null' => FALSE,
        ),
        'format' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'audio_video',
        ),
      ),
    ),
    'field_source_media_url' => array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'max_length' => '255',
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_source_media_url' => array(
                'value' => 'field_source_media_url_value',
                'format' => 'field_source_media_url_format',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_source_media_url' => array(
                'value' => 'field_source_media_url_value',
                'format' => 'field_source_media_url_format',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'format' => array(
          'table' => 'filter_format',
          'columns' => array(
            'format' => 'format',
          ),
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'field_name' => 'field_source_media_url',
      'type' => 'text',
      'module' => 'text',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'columns' => array(
        'value' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => FALSE,
        ),
        'format' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'audio_video',
        ),
      ),
    ),
    'field_source_media_width' => array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'max_length' => '128',
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_source_media_width' => array(
                'value' => 'field_source_media_width_value',
                'format' => 'field_source_media_width_format',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_source_media_width' => array(
                'value' => 'field_source_media_width_value',
                'format' => 'field_source_media_width_format',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'format' => array(
          'table' => 'filter_format',
          'columns' => array(
            'format' => 'format',
          ),
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'field_name' => 'field_source_media_width',
      'type' => 'text',
      'module' => 'text',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'columns' => array(
        'value' => array(
          'type' => 'varchar',
          'length' => '128',
          'not null' => FALSE,
        ),
        'format' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'audio_video',
        ),
      ),
    ),
    'field_tags' => array(
      'settings' => array(
        'allowed_values' => array(
          0 => array(
            'vocabulary' => 'tags',
            'parent' => 0,
          ),
        ),
        'options_list_callback' => 'i18n_taxonomy_allowed_values',
      ),
      'entity_types' => array(),
      'translatable' => '0',
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_tags' => array(
                'tid' => 'field_tags_tid',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_tags' => array(
                'tid' => 'field_tags_tid',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'tid' => array(
          'table' => 'taxonomy_term_data',
          'columns' => array(
            'tid' => 'tid',
          ),
        ),
      ),
      'indexes' => array(
        'tid' => array(
          0 => 'tid',
        ),
      ),
      'field_name' => 'field_tags',
      'type' => 'taxonomy_term_reference',
      'module' => 'taxonomy',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '-1',
      'deleted' => '0',
      'columns' => array(
        'tid' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => FALSE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'audio_video',
        ),
      ),
    ),
    'field_text_transcript' => array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_text_transcript' => array(
                'value' => 'field_text_transcript_value',
                'format' => 'field_text_transcript_format',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_text_transcript' => array(
                'value' => 'field_text_transcript_value',
                'format' => 'field_text_transcript_format',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'format' => array(
          'table' => 'filter_format',
          'columns' => array(
            'format' => 'format',
          ),
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'field_name' => 'field_text_transcript',
      'type' => 'text_long',
      'module' => 'text',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'columns' => array(
        'value' => array(
          'type' => 'text',
          'size' => 'big',
          'not null' => FALSE,
        ),
        'format' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'audio_video',
        ),
      ),
    ),
    'field_type' => array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'allowed_values' => array(
          0 => array(
            'vocabulary' => 'media_type',
            'parent' => '0',
          ),
        ),
        'options_list_callback' => 'i18n_taxonomy_allowed_values',
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_type' => array(
                'tid' => 'field_type_tid',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_type' => array(
                'tid' => 'field_type_tid',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'tid' => array(
          'table' => 'taxonomy_term_data',
          'columns' => array(
            'tid' => 'tid',
          ),
        ),
      ),
      'indexes' => array(
        'tid' => array(
          0 => 'tid',
        ),
      ),
      'field_name' => 'field_type',
      'type' => 'taxonomy_term_reference',
      'module' => 'taxonomy',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'columns' => array(
        'tid' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => FALSE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'audio_video',
        ),
      ),
    ),
    'field_web_media_bitrate' => array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'max_length' => '255',
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_web_media_bitrate' => array(
                'value' => 'field_web_media_bitrate_value',
                'format' => 'field_web_media_bitrate_format',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_web_media_bitrate' => array(
                'value' => 'field_web_media_bitrate_value',
                'format' => 'field_web_media_bitrate_format',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'format' => array(
          'table' => 'filter_format',
          'columns' => array(
            'format' => 'format',
          ),
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'field_name' => 'field_web_media_bitrate',
      'type' => 'text',
      'module' => 'text',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'columns' => array(
        'value' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => FALSE,
        ),
        'format' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'audio_video',
        ),
      ),
    ),
    'field_web_media_filesize' => array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'max_length' => '255',
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_web_media_filesize' => array(
                'value' => 'field_web_media_filesize_value',
                'format' => 'field_web_media_filesize_format',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_web_media_filesize' => array(
                'value' => 'field_web_media_filesize_value',
                'format' => 'field_web_media_filesize_format',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'format' => array(
          'table' => 'filter_format',
          'columns' => array(
            'format' => 'format',
          ),
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'field_name' => 'field_web_media_filesize',
      'type' => 'text',
      'module' => 'text',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'columns' => array(
        'value' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => FALSE,
        ),
        'format' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'audio_video',
        ),
      ),
    ),
    'field_web_media_height' => array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'max_length' => '128',
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_web_media_height' => array(
                'value' => 'field_web_media_height_value',
                'format' => 'field_web_media_height_format',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_web_media_height' => array(
                'value' => 'field_web_media_height_value',
                'format' => 'field_web_media_height_format',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'format' => array(
          'table' => 'filter_format',
          'columns' => array(
            'format' => 'format',
          ),
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'field_name' => 'field_web_media_height',
      'type' => 'text',
      'module' => 'text',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'columns' => array(
        'value' => array(
          'type' => 'varchar',
          'length' => '128',
          'not null' => FALSE,
        ),
        'format' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'audio_video',
        ),
      ),
    ),
    'field_web_media_url' => array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'max_length' => '255',
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_web_media_url' => array(
                'value' => 'field_web_media_url_value',
                'format' => 'field_web_media_url_format',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_web_media_url' => array(
                'value' => 'field_web_media_url_value',
                'format' => 'field_web_media_url_format',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'format' => array(
          'table' => 'filter_format',
          'columns' => array(
            'format' => 'format',
          ),
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'field_name' => 'field_web_media_url',
      'type' => 'text',
      'module' => 'text',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'columns' => array(
        'value' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => FALSE,
        ),
        'format' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'audio_video',
        ),
      ),
    ),
    'field_web_media_width' => array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'max_length' => '128',
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_web_media_width' => array(
                'value' => 'field_web_media_width_value',
                'format' => 'field_web_media_width_format',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_web_media_width' => array(
                'value' => 'field_web_media_width_value',
                'format' => 'field_web_media_width_format',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'format' => array(
          'table' => 'filter_format',
          'columns' => array(
            'format' => 'format',
          ),
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'field_name' => 'field_web_media_width',
      'type' => 'text',
      'module' => 'text',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'columns' => array(
        'value' => array(
          'type' => 'varchar',
          'length' => '128',
          'not null' => FALSE,
        ),
        'format' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'audio_video',
        ),
      ),
    ),
  ),
  'instances' => array(
    'body' => array(
      0 => array(
        'label' => 'Body',
        'widget' => array(
          'type' => 'text_textarea_with_summary',
          'settings' => array(
            'rows' => 20,
            'summary_rows' => 5,
          ),
          'weight' => '2',
          'module' => 'text',
        ),
        'settings' => array(
          'display_summary' => TRUE,
          'text_processing' => 1,
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'hidden',
            'type' => 'text_default',
            'weight' => '0',
            'settings' => array(),
            'module' => 'text',
          ),
          'teaser' => array(
            'label' => 'hidden',
            'type' => 'text_summary_or_trimmed',
            'weight' => '0',
            'settings' => array(
              'trim_length' => '320',
            ),
            'module' => 'text',
          ),
          'rss' => array(
            'label' => 'hidden',
            'type' => 'text_default',
            'weight' => '0',
            'settings' => array(),
            'module' => 'text',
          ),
          'search_result' => array(
            'label' => 'hidden',
            'type' => 'text_default',
            'weight' => '0',
            'settings' => array(),
            'module' => 'text',
          ),
        ),
        'required' => FALSE,
        'description' => '',
        'default_value' => NULL,
        'field_name' => 'body',
        'entity_type' => 'node',
        'bundle' => 'audio_video',
        'deleted' => '0',
      ),
    ),
    'field_audio_transcript_media_bit' => array(
      0 => array(
        'label' => 'Audio Transcript Media Bitrate',
        'widget' => array(
          'weight' => '8',
          'type' => 'text_textfield',
          'module' => 'text',
          'active' => 1,
          'settings' => array(
            'size' => '60',
          ),
        ),
        'settings' => array(
          'text_processing' => '0',
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'hidden',
            'type' => 'hidden',
            'weight' => '15',
            'settings' => array(),
          ),
          'teaser' => array(
            'label' => 'above',
            'type' => 'hidden',
            'weight' => '0',
            'settings' => array(),
          ),
          'rss' => array(
            'label' => 'hidden',
            'type' => 'hidden',
            'weight' => '15',
            'settings' => array(),
          ),
          'search_result' => array(
            'label' => 'hidden',
            'type' => 'hidden',
            'weight' => '15',
            'settings' => array(),
          ),
        ),
        'required' => 0,
        'description' => '',
        'default_value' => NULL,
        'field_name' => 'field_audio_transcript_media_bit',
        'entity_type' => 'node',
        'bundle' => 'audio_video',
        'deleted' => '0',
      ),
    ),
    'field_audio_transcript_media_fil' => array(
      0 => array(
        'label' => 'Audio Transcript Media Filesize',
        'widget' => array(
          'weight' => '9',
          'type' => 'text_textfield',
          'module' => 'text',
          'active' => 1,
          'settings' => array(
            'size' => '60',
          ),
        ),
        'settings' => array(
          'text_processing' => '0',
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'hidden',
            'type' => 'hidden',
            'weight' => '20',
            'settings' => array(),
          ),
          'teaser' => array(
            'label' => 'above',
            'type' => 'hidden',
            'weight' => '0',
            'settings' => array(),
          ),
          'rss' => array(
            'label' => 'hidden',
            'type' => 'hidden',
            'weight' => '20',
            'settings' => array(),
          ),
          'search_result' => array(
            'label' => 'hidden',
            'type' => 'hidden',
            'weight' => '20',
            'settings' => array(),
          ),
        ),
        'required' => 0,
        'description' => '',
        'default_value' => NULL,
        'field_name' => 'field_audio_transcript_media_fil',
        'entity_type' => 'node',
        'bundle' => 'audio_video',
        'deleted' => '0',
      ),
    ),
    'field_audio_transcript_media_url' => array(
      0 => array(
        'label' => 'Audio Transcript Media URL',
        'widget' => array(
          'weight' => '7',
          'type' => 'text_textfield',
          'module' => 'text',
          'active' => 1,
          'settings' => array(
            'size' => '60',
          ),
        ),
        'settings' => array(
          'text_processing' => '0',
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'hidden',
            'type' => 'hidden',
            'weight' => '11',
            'settings' => array(),
          ),
          'teaser' => array(
            'label' => 'above',
            'type' => 'hidden',
            'weight' => '0',
            'settings' => array(),
          ),
          'rss' => array(
            'label' => 'hidden',
            'type' => 'hidden',
            'weight' => '11',
            'settings' => array(),
          ),
          'search_result' => array(
            'label' => 'hidden',
            'type' => 'hidden',
            'weight' => '11',
            'settings' => array(),
          ),
        ),
        'required' => 0,
        'description' => '',
        'default_value' => NULL,
        'field_name' => 'field_audio_transcript_media_url',
        'entity_type' => 'node',
        'bundle' => 'audio_video',
        'deleted' => '0',
      ),
    ),
    'field_category' => array(
      0 => array(
        'label' => 'Category',
        'widget' => array(
          'weight' => '5',
          'type' => 'taxonomy_autocomplete',
          'module' => 'taxonomy',
          'active' => 0,
          'settings' => array(
            'size' => 60,
            'autocomplete_path' => 'taxonomy/autocomplete',
          ),
        ),
        'settings' => array(
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'inline',
            'type' => 'taxonomy_term_reference_link',
            'weight' => '6',
            'settings' => array(),
            'module' => 'taxonomy',
          ),
          'teaser' => array(
            'label' => 'above',
            'type' => 'hidden',
            'weight' => '0',
            'settings' => array(),
          ),
          'rss' => array(
            'label' => 'hidden',
            'type' => 'taxonomy_term_reference_link',
            'weight' => '6',
            'settings' => array(),
            'module' => 'taxonomy',
          ),
          'search_result' => array(
            'label' => 'inline',
            'type' => 'taxonomy_term_reference_link',
            'weight' => '6',
            'settings' => array(),
            'module' => 'taxonomy',
          ),
        ),
        'required' => 0,
        'description' => '',
        'default_value' => NULL,
        'field_name' => 'field_category',
        'entity_type' => 'node',
        'bundle' => 'audio_video',
        'deleted' => '0',
      ),
    ),
    'field_credit' => array(
      0 => array(
        'label' => 'Credit',
        'widget' => array(
          'weight' => '12',
          'type' => 'text_textfield',
          'module' => 'text',
          'active' => 1,
          'settings' => array(
            'size' => '60',
          ),
        ),
        'settings' => array(
          'text_processing' => '0',
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'above',
            'type' => 'text_default',
            'weight' => '24',
            'settings' => array(),
            'module' => 'text',
          ),
          'teaser' => array(
            'label' => 'above',
            'type' => 'hidden',
            'weight' => '0',
            'settings' => array(),
          ),
          'rss' => array(
            'label' => 'above',
            'type' => 'text_default',
            'weight' => '24',
            'settings' => array(),
            'module' => 'text',
          ),
          'search_result' => array(
            'label' => 'above',
            'type' => 'text_default',
            'weight' => '24',
            'settings' => array(),
            'module' => 'text',
          ),
        ),
        'required' => 0,
        'description' => 'Attribute the content to another author or organization.',
        'default_value' => NULL,
        'field_name' => 'field_credit',
        'entity_type' => 'node',
        'bundle' => 'audio_video',
        'deleted' => '0',
      ),
    ),
    'field_cxp_id' => array(
      0 => array(
        'label' => 'CXP ID',
        'widget' => array(
          'weight' => '23',
          'type' => 'text_textfield',
          'module' => 'text',
          'active' => 1,
          'settings' => array(
            'size' => '60',
          ),
        ),
        'settings' => array(
          'text_processing' => '0',
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'above',
            'type' => 'text_default',
            'weight' => '38',
            'settings' => array(),
            'module' => 'text',
          ),
          'teaser' => array(
            'label' => 'above',
            'type' => 'hidden',
            'weight' => '0',
            'settings' => array(),
          ),
          'rss' => array(
            'type' => 'hidden',
            'label' => 'above',
            'settings' => array(),
            'weight' => 0,
          ),
          'search_result' => array(
            'type' => 'hidden',
            'label' => 'above',
            'settings' => array(),
            'weight' => 0,
          ),
        ),
        'required' => 0,
        'description' => 'Unique numeric identifier for content items on the BBG\'s ContentExchangePlatform',
        'default_value' => NULL,
        'field_name' => 'field_cxp_id',
        'entity_type' => 'node',
        'bundle' => 'audio_video',
        'deleted' => '0',
      ),
    ),
    'field_display_date' => array(
      0 => array(
        'label' => 'Display Date',
        'widget' => array(
          'weight' => '3',
          'type' => 'text_textfield',
          'module' => 'text',
          'active' => 1,
          'settings' => array(
            'size' => '60',
          ),
        ),
        'settings' => array(
          'text_processing' => '0',
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'above',
            'type' => 'text_default',
            'weight' => '31',
            'settings' => array(),
            'module' => 'text',
          ),
          'rss' => array(
            'label' => 'above',
            'type' => 'text_default',
            'weight' => '31',
            'settings' => array(),
            'module' => 'text',
          ),
          'search_result' => array(
            'label' => 'above',
            'type' => 'text_default',
            'weight' => '31',
            'settings' => array(),
            'module' => 'text',
          ),
          'teaser' => array(
            'label' => 'above',
            'type' => 'hidden',
            'weight' => '0',
            'settings' => array(),
          ),
        ),
        'required' => 0,
        'description' => 'Content\'s broadcast date (Unix timestamp)',
        'default_value' => NULL,
        'field_name' => 'field_display_date',
        'entity_type' => 'node',
        'bundle' => 'audio_video',
        'deleted' => '0',
      ),
    ),
    'field_high_quality_media_bitrate' => array(
      0 => array(
        'label' => 'High Quality Media Bitrate',
        'widget' => array(
          'weight' => '22',
          'type' => 'text_textfield',
          'module' => 'text',
          'active' => 1,
          'settings' => array(
            'size' => '60',
          ),
        ),
        'settings' => array(
          'text_processing' => '0',
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'above',
            'type' => 'text_default',
            'weight' => '33',
            'settings' => array(),
            'module' => 'text',
          ),
          'rss' => array(
            'label' => 'above',
            'type' => 'text_default',
            'weight' => '33',
            'settings' => array(),
            'module' => 'text',
          ),
          'search_result' => array(
            'label' => 'above',
            'type' => 'text_default',
            'weight' => '33',
            'settings' => array(),
            'module' => 'text',
          ),
          'teaser' => array(
            'label' => 'above',
            'type' => 'hidden',
            'weight' => '0',
            'settings' => array(),
          ),
        ),
        'required' => 0,
        'description' => '',
        'default_value' => NULL,
        'field_name' => 'field_high_quality_media_bitrate',
        'entity_type' => 'node',
        'bundle' => 'audio_video',
        'deleted' => '0',
      ),
    ),
    'field_high_quality_media_filesiz' => array(
      0 => array(
        'label' => 'High Quality Media Filesize',
        'widget' => array(
          'weight' => '23',
          'type' => 'text_textfield',
          'module' => 'text',
          'active' => 1,
          'settings' => array(
            'size' => '60',
          ),
        ),
        'settings' => array(
          'text_processing' => '0',
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'above',
            'type' => 'text_default',
            'weight' => '34',
            'settings' => array(),
            'module' => 'text',
          ),
          'rss' => array(
            'label' => 'above',
            'type' => 'text_default',
            'weight' => '34',
            'settings' => array(),
            'module' => 'text',
          ),
          'search_result' => array(
            'label' => 'above',
            'type' => 'text_default',
            'weight' => '34',
            'settings' => array(),
            'module' => 'text',
          ),
          'teaser' => array(
            'label' => 'above',
            'type' => 'hidden',
            'weight' => '0',
            'settings' => array(),
          ),
        ),
        'required' => 0,
        'description' => '',
        'default_value' => NULL,
        'field_name' => 'field_high_quality_media_filesiz',
        'entity_type' => 'node',
        'bundle' => 'audio_video',
        'deleted' => '0',
      ),
    ),
    'field_high_quality_media_height' => array(
      0 => array(
        'label' => 'High Quality Media Height',
        'widget' => array(
          'weight' => '25',
          'type' => 'text_textfield',
          'module' => 'text',
          'active' => 1,
          'settings' => array(
            'size' => '60',
          ),
        ),
        'settings' => array(
          'text_processing' => '0',
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'above',
            'type' => 'text_default',
            'weight' => '44',
            'settings' => array(),
            'module' => 'text',
          ),
          'teaser' => array(
            'label' => 'above',
            'type' => 'hidden',
            'weight' => '0',
            'settings' => array(),
          ),
          'rss' => array(
            'type' => 'hidden',
            'label' => 'above',
            'settings' => array(),
            'weight' => 0,
          ),
          'search_result' => array(
            'type' => 'hidden',
            'label' => 'above',
            'settings' => array(),
            'weight' => 0,
          ),
        ),
        'required' => 0,
        'description' => '',
        'default_value' => NULL,
        'field_name' => 'field_high_quality_media_height',
        'entity_type' => 'node',
        'bundle' => 'audio_video',
        'deleted' => '0',
      ),
    ),
    'field_high_quality_media_url' => array(
      0 => array(
        'label' => 'High Quality Media URL',
        'widget' => array(
          'weight' => '21',
          'type' => 'text_textfield',
          'module' => 'text',
          'active' => 1,
          'settings' => array(
            'size' => '60',
          ),
        ),
        'settings' => array(
          'text_processing' => '0',
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'above',
            'type' => 'text_default',
            'weight' => '32',
            'settings' => array(),
            'module' => 'text',
          ),
          'rss' => array(
            'label' => 'above',
            'type' => 'text_default',
            'weight' => '32',
            'settings' => array(),
            'module' => 'text',
          ),
          'search_result' => array(
            'label' => 'above',
            'type' => 'text_default',
            'weight' => '32',
            'settings' => array(),
            'module' => 'text',
          ),
          'teaser' => array(
            'label' => 'above',
            'type' => 'hidden',
            'weight' => '0',
            'settings' => array(),
          ),
        ),
        'required' => 0,
        'description' => '',
        'default_value' => NULL,
        'field_name' => 'field_high_quality_media_url',
        'entity_type' => 'node',
        'bundle' => 'audio_video',
        'deleted' => '0',
      ),
    ),
    'field_high_quality_media_width' => array(
      0 => array(
        'label' => 'High Quality Media Width',
        'widget' => array(
          'weight' => '24',
          'type' => 'text_textfield',
          'module' => 'text',
          'active' => 1,
          'settings' => array(
            'size' => '60',
          ),
        ),
        'settings' => array(
          'text_processing' => '0',
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'above',
            'type' => 'text_default',
            'weight' => '43',
            'settings' => array(),
            'module' => 'text',
          ),
          'teaser' => array(
            'label' => 'above',
            'type' => 'hidden',
            'weight' => '0',
            'settings' => array(),
          ),
          'rss' => array(
            'type' => 'hidden',
            'label' => 'above',
            'settings' => array(),
            'weight' => 0,
          ),
          'search_result' => array(
            'type' => 'hidden',
            'label' => 'above',
            'settings' => array(),
            'weight' => 0,
          ),
        ),
        'required' => 0,
        'description' => '',
        'default_value' => NULL,
        'field_name' => 'field_high_quality_media_width',
        'entity_type' => 'node',
        'bundle' => 'audio_video',
        'deleted' => '0',
      ),
    ),
    'field_image_height' => array(
      0 => array(
        'label' => 'Image Height',
        'widget' => array(
          'weight' => '3',
          'type' => 'text_textfield',
          'module' => 'text',
          'active' => 1,
          'settings' => array(
            'size' => '60',
          ),
        ),
        'settings' => array(
          'text_processing' => '0',
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'hidden',
            'type' => 'hidden',
            'weight' => '14',
            'settings' => array(),
          ),
          'teaser' => array(
            'label' => 'above',
            'type' => 'hidden',
            'weight' => '0',
            'settings' => array(),
          ),
          'rss' => array(
            'label' => 'hidden',
            'type' => 'hidden',
            'weight' => '14',
            'settings' => array(),
          ),
          'search_result' => array(
            'label' => 'hidden',
            'type' => 'hidden',
            'weight' => '14',
            'settings' => array(),
          ),
        ),
        'required' => 0,
        'description' => '',
        'default_value' => NULL,
        'field_name' => 'field_image_height',
        'entity_type' => 'node',
        'bundle' => 'audio_video',
        'deleted' => '0',
      ),
    ),
    'field_image_url' => array(
      0 => array(
        'label' => 'Image URL',
        'widget' => array(
          'weight' => '1',
          'type' => 'text_textfield',
          'module' => 'text',
          'active' => 1,
          'settings' => array(
            'size' => '60',
          ),
        ),
        'settings' => array(
          'text_processing' => '0',
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'hidden',
            'type' => 'text_default',
            'weight' => '5',
            'settings' => array(
              'imagecache_external_style' => 'large',
              'imagecache_external_link' => 'content',
            ),
            'module' => 'text',
          ),
          'teaser' => array(
            'label' => 'above',
            'type' => 'hidden',
            'weight' => '0',
            'settings' => array(),
          ),
          'rss' => array(
            'label' => 'hidden',
            'type' => 'text_default',
            'weight' => '5',
            'settings' => array(
              'imagecache_external_style' => 'large',
              'imagecache_external_link' => 'content',
            ),
            'module' => 'text',
          ),
          'search_result' => array(
            'label' => 'hidden',
            'type' => 'text_default',
            'weight' => '5',
            'settings' => array(
              'imagecache_external_style' => 'large',
              'imagecache_external_link' => 'content',
            ),
            'module' => 'text',
          ),
        ),
        'required' => 0,
        'description' => '',
        'default_value' => NULL,
        'field_name' => 'field_image_url',
        'entity_type' => 'node',
        'bundle' => 'audio_video',
        'deleted' => '0',
      ),
    ),
    'field_image_width' => array(
      0 => array(
        'label' => 'Image Width',
        'widget' => array(
          'weight' => '2',
          'type' => 'text_textfield',
          'module' => 'text',
          'active' => 1,
          'settings' => array(
            'size' => '60',
          ),
        ),
        'settings' => array(
          'text_processing' => '0',
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'hidden',
            'type' => 'hidden',
            'weight' => '13',
            'settings' => array(),
          ),
          'teaser' => array(
            'label' => 'above',
            'type' => 'hidden',
            'weight' => '0',
            'settings' => array(),
          ),
          'rss' => array(
            'label' => 'hidden',
            'type' => 'hidden',
            'weight' => '13',
            'settings' => array(),
          ),
          'search_result' => array(
            'label' => 'hidden',
            'type' => 'hidden',
            'weight' => '13',
            'settings' => array(),
          ),
        ),
        'required' => 0,
        'description' => '',
        'default_value' => NULL,
        'field_name' => 'field_image_width',
        'entity_type' => 'node',
        'bundle' => 'audio_video',
        'deleted' => '0',
      ),
    ),
    'field_import_id' => array(
      0 => array(
        'label' => 'Import ID',
        'widget' => array(
          'weight' => '20',
          'type' => 'text_textfield',
          'module' => 'text',
          'active' => 1,
          'settings' => array(
            'size' => '60',
          ),
        ),
        'settings' => array(
          'text_processing' => '0',
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'hidden',
            'type' => 'hidden',
            'weight' => '12',
            'settings' => array(),
          ),
          'teaser' => array(
            'label' => 'above',
            'type' => 'hidden',
            'weight' => '0',
            'settings' => array(),
          ),
          'rss' => array(
            'label' => 'hidden',
            'type' => 'hidden',
            'weight' => '12',
            'settings' => array(),
          ),
          'search_result' => array(
            'label' => 'hidden',
            'type' => 'hidden',
            'weight' => '12',
            'settings' => array(),
          ),
        ),
        'required' => 0,
        'description' => '',
        'default_value' => NULL,
        'field_name' => 'field_import_id',
        'entity_type' => 'node',
        'bundle' => 'audio_video',
        'deleted' => '0',
      ),
    ),
    'field_import_source' => array(
      0 => array(
        'label' => 'Import Source',
        'widget' => array(
          'weight' => '21',
          'type' => 'text_textfield',
          'module' => 'text',
          'active' => 1,
          'settings' => array(
            'size' => '60',
          ),
        ),
        'settings' => array(
          'text_processing' => '0',
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'above',
            'type' => 'text_default',
            'weight' => '27',
            'settings' => array(),
            'module' => 'text',
          ),
          'teaser' => array(
            'label' => 'above',
            'type' => 'hidden',
            'weight' => '0',
            'settings' => array(),
          ),
          'rss' => array(
            'label' => 'above',
            'type' => 'text_default',
            'weight' => '27',
            'settings' => array(),
            'module' => 'text',
          ),
          'search_result' => array(
            'label' => 'above',
            'type' => 'text_default',
            'weight' => '27',
            'settings' => array(),
            'module' => 'text',
          ),
        ),
        'required' => 0,
        'description' => '',
        'default_value' => NULL,
        'field_name' => 'field_import_source',
        'entity_type' => 'node',
        'bundle' => 'audio_video',
        'deleted' => '0',
      ),
    ),
    'field_media_duration' => array(
      0 => array(
        'label' => 'Media Duration',
        'widget' => array(
          'weight' => '11',
          'type' => 'text_textfield',
          'module' => 'text',
          'active' => 1,
          'settings' => array(
            'size' => '60',
          ),
        ),
        'settings' => array(
          'text_processing' => '0',
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'inline',
            'type' => 'text_default',
            'weight' => '21',
            'settings' => array(),
            'module' => 'text',
          ),
          'teaser' => array(
            'label' => 'above',
            'type' => 'hidden',
            'weight' => '0',
            'settings' => array(),
          ),
          'rss' => array(
            'label' => 'hidden',
            'type' => 'text_default',
            'weight' => '21',
            'settings' => array(),
            'module' => 'text',
          ),
          'search_result' => array(
            'label' => 'inline',
            'type' => 'text_default',
            'weight' => '21',
            'settings' => array(),
            'module' => 'text',
          ),
        ),
        'required' => 0,
        'description' => '',
        'default_value' => NULL,
        'field_name' => 'field_media_duration',
        'entity_type' => 'node',
        'bundle' => 'audio_video',
        'deleted' => '0',
      ),
    ),
    'field_mobile_media_bitrate' => array(
      0 => array(
        'label' => 'Mobile Media Bitrate',
        'widget' => array(
          'weight' => '19',
          'type' => 'text_textfield',
          'module' => 'text',
          'active' => 1,
          'settings' => array(
            'size' => '60',
          ),
        ),
        'settings' => array(
          'text_processing' => '0',
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'above',
            'type' => 'text_default',
            'weight' => '29',
            'settings' => array(),
            'module' => 'text',
          ),
          'teaser' => array(
            'label' => 'above',
            'type' => 'hidden',
            'weight' => '0',
            'settings' => array(),
          ),
          'rss' => array(
            'label' => 'above',
            'type' => 'text_default',
            'weight' => '29',
            'settings' => array(),
            'module' => 'text',
          ),
          'search_result' => array(
            'label' => 'above',
            'type' => 'text_default',
            'weight' => '29',
            'settings' => array(),
            'module' => 'text',
          ),
        ),
        'required' => 0,
        'description' => '',
        'default_value' => NULL,
        'field_name' => 'field_mobile_media_bitrate',
        'entity_type' => 'node',
        'bundle' => 'audio_video',
        'deleted' => '0',
      ),
    ),
    'field_mobile_media_filesize' => array(
      0 => array(
        'label' => 'Mobile Media Filesize',
        'widget' => array(
          'weight' => '20',
          'type' => 'text_textfield',
          'module' => 'text',
          'active' => 1,
          'settings' => array(
            'size' => '60',
          ),
        ),
        'settings' => array(
          'text_processing' => '0',
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'above',
            'type' => 'text_default',
            'weight' => '30',
            'settings' => array(),
            'module' => 'text',
          ),
          'teaser' => array(
            'label' => 'above',
            'type' => 'hidden',
            'weight' => '0',
            'settings' => array(),
          ),
          'rss' => array(
            'label' => 'above',
            'type' => 'text_default',
            'weight' => '30',
            'settings' => array(),
            'module' => 'text',
          ),
          'search_result' => array(
            'label' => 'above',
            'type' => 'text_default',
            'weight' => '30',
            'settings' => array(),
            'module' => 'text',
          ),
        ),
        'required' => 0,
        'description' => '',
        'default_value' => NULL,
        'field_name' => 'field_mobile_media_filesize',
        'entity_type' => 'node',
        'bundle' => 'audio_video',
        'deleted' => '0',
      ),
    ),
    'field_mobile_media_height' => array(
      0 => array(
        'label' => 'Mobile Media Height',
        'widget' => array(
          'weight' => '22',
          'type' => 'text_textfield',
          'module' => 'text',
          'active' => 1,
          'settings' => array(
            'size' => '60',
          ),
        ),
        'settings' => array(
          'text_processing' => '0',
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'above',
            'type' => 'text_default',
            'weight' => '40',
            'settings' => array(),
            'module' => 'text',
          ),
          'teaser' => array(
            'label' => 'above',
            'type' => 'hidden',
            'weight' => '0',
            'settings' => array(),
          ),
          'rss' => array(
            'type' => 'hidden',
            'label' => 'above',
            'settings' => array(),
            'weight' => 0,
          ),
          'search_result' => array(
            'type' => 'hidden',
            'label' => 'above',
            'settings' => array(),
            'weight' => 0,
          ),
        ),
        'required' => 0,
        'description' => '',
        'default_value' => NULL,
        'field_name' => 'field_mobile_media_height',
        'entity_type' => 'node',
        'bundle' => 'audio_video',
        'deleted' => '0',
      ),
    ),
    'field_mobile_media_url' => array(
      0 => array(
        'label' => 'Mobile Media URL',
        'widget' => array(
          'weight' => '18',
          'type' => 'text_textfield',
          'module' => 'text',
          'active' => 1,
          'settings' => array(
            'size' => '60',
          ),
        ),
        'settings' => array(
          'text_processing' => '0',
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'above',
            'type' => 'text_default',
            'weight' => '28',
            'settings' => array(),
            'module' => 'text',
          ),
          'teaser' => array(
            'label' => 'above',
            'type' => 'hidden',
            'weight' => '0',
            'settings' => array(),
          ),
          'rss' => array(
            'label' => 'above',
            'type' => 'text_default',
            'weight' => '28',
            'settings' => array(),
            'module' => 'text',
          ),
          'search_result' => array(
            'label' => 'above',
            'type' => 'text_default',
            'weight' => '28',
            'settings' => array(),
            'module' => 'text',
          ),
        ),
        'required' => 0,
        'description' => 'The mobile quality url',
        'default_value' => NULL,
        'field_name' => 'field_mobile_media_url',
        'entity_type' => 'node',
        'bundle' => 'audio_video',
        'deleted' => '0',
      ),
    ),
    'field_mobile_media_width' => array(
      0 => array(
        'label' => 'Mobile Media Width',
        'widget' => array(
          'weight' => '21',
          'type' => 'text_textfield',
          'module' => 'text',
          'active' => 1,
          'settings' => array(
            'size' => '60',
          ),
        ),
        'settings' => array(
          'text_processing' => '0',
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'above',
            'type' => 'text_default',
            'weight' => '39',
            'settings' => array(),
            'module' => 'text',
          ),
          'teaser' => array(
            'label' => 'above',
            'type' => 'hidden',
            'weight' => '0',
            'settings' => array(),
          ),
          'rss' => array(
            'type' => 'hidden',
            'label' => 'above',
            'settings' => array(),
            'weight' => 0,
          ),
          'search_result' => array(
            'type' => 'hidden',
            'label' => 'above',
            'settings' => array(),
            'weight' => 0,
          ),
        ),
        'required' => 0,
        'description' => '',
        'default_value' => NULL,
        'field_name' => 'field_mobile_media_width',
        'entity_type' => 'node',
        'bundle' => 'audio_video',
        'deleted' => '0',
      ),
    ),
    'field_network' => array(
      0 => array(
        'label' => 'Network',
        'widget' => array(
          'weight' => '8',
          'type' => 'options_select',
          'module' => 'options',
          'active' => 1,
          'settings' => array(),
        ),
        'settings' => array(
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'above',
            'type' => 'taxonomy_term_reference_link',
            'weight' => '26',
            'settings' => array(),
            'module' => 'taxonomy',
          ),
          'teaser' => array(
            'label' => 'above',
            'type' => 'hidden',
            'weight' => '0',
            'settings' => array(),
          ),
          'rss' => array(
            'label' => 'above',
            'type' => 'taxonomy_term_reference_link',
            'weight' => '26',
            'settings' => array(),
            'module' => 'taxonomy',
          ),
          'search_result' => array(
            'label' => 'above',
            'type' => 'taxonomy_term_reference_link',
            'weight' => '26',
            'settings' => array(),
            'module' => 'taxonomy',
          ),
        ),
        'required' => 0,
        'description' => '',
        'default_value' => NULL,
        'field_name' => 'field_network',
        'entity_type' => 'node',
        'bundle' => 'audio_video',
        'deleted' => '0',
      ),
    ),
    'field_programs' => array(
      0 => array(
        'label' => 'Programs',
        'widget' => array(
          'weight' => '6',
          'type' => 'taxonomy_autocomplete',
          'module' => 'taxonomy',
          'active' => 0,
          'settings' => array(
            'size' => 60,
            'autocomplete_path' => 'taxonomy/autocomplete',
          ),
        ),
        'settings' => array(
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'above',
            'type' => 'taxonomy_term_reference_link',
            'weight' => '23',
            'settings' => array(),
            'module' => 'taxonomy',
          ),
          'teaser' => array(
            'label' => 'above',
            'type' => 'hidden',
            'weight' => '0',
            'settings' => array(),
          ),
          'rss' => array(
            'label' => 'above',
            'type' => 'taxonomy_term_reference_link',
            'weight' => '23',
            'settings' => array(),
            'module' => 'taxonomy',
          ),
          'search_result' => array(
            'label' => 'above',
            'type' => 'taxonomy_term_reference_link',
            'weight' => '23',
            'settings' => array(),
            'module' => 'taxonomy',
          ),
        ),
        'required' => 0,
        'description' => '',
        'default_value' => NULL,
        'field_name' => 'field_programs',
        'entity_type' => 'node',
        'bundle' => 'audio_video',
        'deleted' => '0',
      ),
    ),
    'field_region' => array(
      0 => array(
        'label' => 'Region',
        'widget' => array(
          'weight' => '7',
          'type' => 'taxonomy_autocomplete',
          'module' => 'taxonomy',
          'active' => 0,
          'settings' => array(
            'size' => 60,
            'autocomplete_path' => 'taxonomy/autocomplete',
          ),
        ),
        'settings' => array(
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'above',
            'type' => 'taxonomy_term_reference_link',
            'weight' => '25',
            'settings' => array(),
            'module' => 'taxonomy',
          ),
          'teaser' => array(
            'label' => 'above',
            'type' => 'hidden',
            'weight' => '0',
            'settings' => array(),
          ),
          'rss' => array(
            'label' => 'above',
            'type' => 'taxonomy_term_reference_link',
            'weight' => '25',
            'settings' => array(),
            'module' => 'taxonomy',
          ),
          'search_result' => array(
            'label' => 'above',
            'type' => 'taxonomy_term_reference_link',
            'weight' => '25',
            'settings' => array(),
            'module' => 'taxonomy',
          ),
        ),
        'required' => 0,
        'description' => '',
        'default_value' => NULL,
        'field_name' => 'field_region',
        'entity_type' => 'node',
        'bundle' => 'audio_video',
        'deleted' => '0',
      ),
    ),
    'field_source_media_bitrate' => array(
      0 => array(
        'label' => 'Source Media Bitrate',
        'widget' => array(
          'type' => 'text_textfield',
          'weight' => '23',
          'settings' => array(
            'size' => 60,
          ),
          'module' => 'text',
        ),
        'settings' => array(
          'text_processing' => 0,
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'above',
            'type' => 'text_default',
            'weight' => '36',
            'settings' => array(),
            'module' => 'text',
          ),
          'rss' => array(
            'label' => 'above',
            'type' => 'text_default',
            'weight' => '36',
            'settings' => array(),
            'module' => 'text',
          ),
          'search_result' => array(
            'label' => 'above',
            'type' => 'text_default',
            'weight' => '36',
            'settings' => array(),
            'module' => 'text',
          ),
          'teaser' => array(
            'label' => 'above',
            'type' => 'hidden',
            'weight' => '0',
            'settings' => array(),
          ),
        ),
        'required' => FALSE,
        'description' => '',
        'default_value' => NULL,
        'field_name' => 'field_source_media_bitrate',
        'entity_type' => 'node',
        'bundle' => 'audio_video',
        'deleted' => '0',
      ),
    ),
    'field_source_media_filesize' => array(
      0 => array(
        'label' => 'Source Media Filesize',
        'widget' => array(
          'weight' => '24',
          'type' => 'text_textfield',
          'module' => 'text',
          'active' => 1,
          'settings' => array(
            'size' => '60',
          ),
        ),
        'settings' => array(
          'text_processing' => '0',
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'above',
            'type' => 'text_default',
            'weight' => '37',
            'settings' => array(),
            'module' => 'text',
          ),
          'rss' => array(
            'label' => 'above',
            'type' => 'text_default',
            'weight' => '37',
            'settings' => array(),
            'module' => 'text',
          ),
          'search_result' => array(
            'label' => 'above',
            'type' => 'text_default',
            'weight' => '37',
            'settings' => array(),
            'module' => 'text',
          ),
          'teaser' => array(
            'label' => 'above',
            'type' => 'hidden',
            'weight' => '0',
            'settings' => array(),
          ),
        ),
        'required' => 0,
        'description' => '',
        'default_value' => NULL,
        'field_name' => 'field_source_media_filesize',
        'entity_type' => 'node',
        'bundle' => 'audio_video',
        'deleted' => '0',
      ),
    ),
    'field_source_media_height' => array(
      0 => array(
        'label' => 'Source Media Height',
        'widget' => array(
          'weight' => '26',
          'type' => 'text_textfield',
          'module' => 'text',
          'active' => 1,
          'settings' => array(
            'size' => '60',
          ),
        ),
        'settings' => array(
          'text_processing' => '0',
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'above',
            'type' => 'text_default',
            'weight' => '46',
            'settings' => array(),
            'module' => 'text',
          ),
          'teaser' => array(
            'label' => 'above',
            'type' => 'hidden',
            'weight' => '0',
            'settings' => array(),
          ),
          'rss' => array(
            'type' => 'hidden',
            'label' => 'above',
            'settings' => array(),
            'weight' => 0,
          ),
          'search_result' => array(
            'type' => 'hidden',
            'label' => 'above',
            'settings' => array(),
            'weight' => 0,
          ),
        ),
        'required' => 0,
        'description' => '',
        'default_value' => NULL,
        'field_name' => 'field_source_media_height',
        'entity_type' => 'node',
        'bundle' => 'audio_video',
        'deleted' => '0',
      ),
    ),
    'field_source_media_url' => array(
      0 => array(
        'label' => 'Source Media URL',
        'widget' => array(
          'weight' => '22',
          'type' => 'text_textfield',
          'module' => 'text',
          'active' => 1,
          'settings' => array(
            'size' => '60',
          ),
        ),
        'settings' => array(
          'text_processing' => '0',
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'above',
            'type' => 'text_default',
            'weight' => '35',
            'settings' => array(),
            'module' => 'text',
          ),
          'rss' => array(
            'label' => 'above',
            'type' => 'text_default',
            'weight' => '35',
            'settings' => array(),
            'module' => 'text',
          ),
          'search_result' => array(
            'label' => 'above',
            'type' => 'text_default',
            'weight' => '35',
            'settings' => array(),
            'module' => 'text',
          ),
          'teaser' => array(
            'label' => 'above',
            'type' => 'hidden',
            'weight' => '0',
            'settings' => array(),
          ),
        ),
        'required' => 0,
        'description' => '',
        'default_value' => NULL,
        'field_name' => 'field_source_media_url',
        'entity_type' => 'node',
        'bundle' => 'audio_video',
        'deleted' => '0',
      ),
    ),
    'field_source_media_width' => array(
      0 => array(
        'label' => 'Source Media Width',
        'widget' => array(
          'weight' => '25',
          'type' => 'text_textfield',
          'module' => 'text',
          'active' => 1,
          'settings' => array(
            'size' => '60',
          ),
        ),
        'settings' => array(
          'text_processing' => '0',
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'above',
            'type' => 'text_default',
            'weight' => '45',
            'settings' => array(),
            'module' => 'text',
          ),
          'teaser' => array(
            'label' => 'above',
            'type' => 'hidden',
            'weight' => '0',
            'settings' => array(),
          ),
          'rss' => array(
            'type' => 'hidden',
            'label' => 'above',
            'settings' => array(),
            'weight' => 0,
          ),
          'search_result' => array(
            'type' => 'hidden',
            'label' => 'above',
            'settings' => array(),
            'weight' => 0,
          ),
        ),
        'required' => 0,
        'description' => '',
        'default_value' => NULL,
        'field_name' => 'field_source_media_width',
        'entity_type' => 'node',
        'bundle' => 'audio_video',
        'deleted' => '0',
      ),
    ),
    'field_tags' => array(
      0 => array(
        'label' => 'Tags',
        'widget' => array(
          'weight' => '9',
          'type' => 'taxonomy_autocomplete',
          'module' => 'taxonomy',
          'active' => 0,
          'settings' => array(
            'size' => 60,
            'autocomplete_path' => 'taxonomy/autocomplete',
          ),
        ),
        'settings' => array(
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'above',
            'type' => 'taxonomy_term_reference_link',
            'weight' => '7',
            'settings' => array(),
            'module' => 'taxonomy',
          ),
          'teaser' => array(
            'label' => 'above',
            'type' => 'hidden',
            'weight' => '0',
            'settings' => array(),
          ),
          'rss' => array(
            'label' => 'hidden',
            'type' => 'taxonomy_term_reference_link',
            'weight' => '7',
            'settings' => array(),
            'module' => 'taxonomy',
          ),
          'search_result' => array(
            'label' => 'above',
            'type' => 'taxonomy_term_reference_link',
            'weight' => '7',
            'settings' => array(),
            'module' => 'taxonomy',
          ),
        ),
        'required' => 0,
        'description' => '',
        'default_value' => NULL,
        'field_name' => 'field_tags',
        'entity_type' => 'node',
        'bundle' => 'audio_video',
        'deleted' => '0',
      ),
    ),
    'field_text_transcript' => array(
      0 => array(
        'label' => 'Text Transcript',
        'widget' => array(
          'weight' => '10',
          'type' => 'text_textarea',
          'module' => 'text',
          'active' => 1,
          'settings' => array(
            'rows' => '5',
          ),
        ),
        'settings' => array(
          'text_processing' => '0',
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'above',
            'type' => 'text_default',
            'weight' => '10',
            'settings' => array(),
            'module' => 'text',
          ),
          'teaser' => array(
            'label' => 'above',
            'type' => 'hidden',
            'weight' => '0',
            'settings' => array(),
          ),
          'rss' => array(
            'label' => 'hidden',
            'type' => 'text_default',
            'weight' => '10',
            'settings' => array(),
            'module' => 'text',
          ),
          'search_result' => array(
            'label' => 'above',
            'type' => 'text_default',
            'weight' => '10',
            'settings' => array(),
            'module' => 'text',
          ),
        ),
        'required' => 0,
        'description' => 'Text transcript for a related media content item.',
        'default_value' => NULL,
        'field_name' => 'field_text_transcript',
        'entity_type' => 'node',
        'bundle' => 'audio_video',
        'deleted' => '0',
      ),
    ),
    'field_type' => array(
      0 => array(
        'label' => 'Media Type',
        'widget' => array(
          'weight' => '4',
          'type' => 'options_select',
          'module' => 'options',
          'active' => 1,
          'settings' => array(),
        ),
        'settings' => array(
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'hidden',
            'type' => 'hidden',
            'weight' => '8',
            'settings' => array(),
          ),
          'teaser' => array(
            'label' => 'above',
            'type' => 'hidden',
            'weight' => '0',
            'settings' => array(),
          ),
          'rss' => array(
            'label' => 'hidden',
            'type' => 'hidden',
            'weight' => '8',
            'settings' => array(),
          ),
          'search_result' => array(
            'label' => 'hidden',
            'type' => 'hidden',
            'weight' => '8',
            'settings' => array(),
          ),
        ),
        'required' => 0,
        'description' => 'Media type (Audio, Video, etc.)',
        'default_value' => NULL,
        'field_name' => 'field_type',
        'entity_type' => 'node',
        'bundle' => 'audio_video',
        'deleted' => '0',
      ),
    ),
    'field_web_media_bitrate' => array(
      0 => array(
        'label' => 'Web Media Bitrate',
        'widget' => array(
          'weight' => '6',
          'type' => 'text_textfield',
          'module' => 'text',
          'active' => 1,
          'settings' => array(
            'size' => '60',
          ),
        ),
        'settings' => array(
          'text_processing' => '0',
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'hidden',
            'type' => 'hidden',
            'weight' => '16',
            'settings' => array(),
          ),
          'teaser' => array(
            'label' => 'above',
            'type' => 'hidden',
            'weight' => '0',
            'settings' => array(),
          ),
          'rss' => array(
            'label' => 'hidden',
            'type' => 'hidden',
            'weight' => '16',
            'settings' => array(),
          ),
          'search_result' => array(
            'label' => 'hidden',
            'type' => 'hidden',
            'weight' => '16',
            'settings' => array(),
          ),
        ),
        'required' => 0,
        'description' => '',
        'default_value' => NULL,
        'field_name' => 'field_web_media_bitrate',
        'entity_type' => 'node',
        'bundle' => 'audio_video',
        'deleted' => '0',
      ),
    ),
    'field_web_media_filesize' => array(
      0 => array(
        'label' => 'Web Media Filesize',
        'widget' => array(
          'weight' => '7',
          'type' => 'text_textfield',
          'module' => 'text',
          'active' => 1,
          'settings' => array(
            'size' => '60',
          ),
        ),
        'settings' => array(
          'text_processing' => '0',
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'hidden',
            'type' => 'hidden',
            'weight' => '19',
            'settings' => array(),
          ),
          'teaser' => array(
            'label' => 'above',
            'type' => 'hidden',
            'weight' => '0',
            'settings' => array(),
          ),
          'rss' => array(
            'label' => 'hidden',
            'type' => 'hidden',
            'weight' => '19',
            'settings' => array(),
          ),
          'search_result' => array(
            'label' => 'hidden',
            'type' => 'hidden',
            'weight' => '19',
            'settings' => array(),
          ),
        ),
        'required' => 0,
        'description' => '',
        'default_value' => NULL,
        'field_name' => 'field_web_media_filesize',
        'entity_type' => 'node',
        'bundle' => 'audio_video',
        'deleted' => '0',
      ),
    ),
    'field_web_media_height' => array(
      0 => array(
        'label' => 'Web Media Height',
        'widget' => array(
          'weight' => '9',
          'type' => 'text_textfield',
          'module' => 'text',
          'active' => 1,
          'settings' => array(
            'size' => '60',
          ),
        ),
        'settings' => array(
          'text_processing' => '0',
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'above',
            'type' => 'text_default',
            'weight' => '42',
            'settings' => array(),
            'module' => 'text',
          ),
          'teaser' => array(
            'label' => 'above',
            'type' => 'hidden',
            'weight' => '0',
            'settings' => array(),
          ),
          'rss' => array(
            'type' => 'hidden',
            'label' => 'above',
            'settings' => array(),
            'weight' => 0,
          ),
          'search_result' => array(
            'type' => 'hidden',
            'label' => 'above',
            'settings' => array(),
            'weight' => 0,
          ),
        ),
        'required' => 0,
        'description' => '',
        'default_value' => NULL,
        'field_name' => 'field_web_media_height',
        'entity_type' => 'node',
        'bundle' => 'audio_video',
        'deleted' => '0',
      ),
    ),
    'field_web_media_url' => array(
      0 => array(
        'label' => 'Web Media URL',
        'widget' => array(
          'weight' => '5',
          'type' => 'text_textfield',
          'module' => 'text',
          'active' => 1,
          'settings' => array(
            'size' => '60',
          ),
        ),
        'settings' => array(
          'text_processing' => '0',
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'hidden',
            'type' => 'hidden',
            'weight' => '9',
            'settings' => array(),
          ),
          'teaser' => array(
            'label' => 'above',
            'type' => 'hidden',
            'weight' => '0',
            'settings' => array(),
          ),
          'rss' => array(
            'label' => 'hidden',
            'type' => 'hidden',
            'weight' => '9',
            'settings' => array(),
          ),
          'search_result' => array(
            'label' => 'hidden',
            'type' => 'hidden',
            'weight' => '9',
            'settings' => array(),
          ),
        ),
        'required' => 0,
        'description' => '',
        'default_value' => NULL,
        'field_name' => 'field_web_media_url',
        'entity_type' => 'node',
        'bundle' => 'audio_video',
        'deleted' => '0',
      ),
    ),
    'field_web_media_width' => array(
      0 => array(
        'label' => 'Web Media Width',
        'widget' => array(
          'weight' => '8',
          'type' => 'text_textfield',
          'module' => 'text',
          'active' => 1,
          'settings' => array(
            'size' => '60',
          ),
        ),
        'settings' => array(
          'text_processing' => '0',
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'above',
            'type' => 'text_default',
            'weight' => '41',
            'settings' => array(),
            'module' => 'text',
          ),
          'teaser' => array(
            'label' => 'above',
            'type' => 'hidden',
            'weight' => '0',
            'settings' => array(),
          ),
          'rss' => array(
            'type' => 'hidden',
            'label' => 'above',
            'settings' => array(),
            'weight' => 0,
          ),
          'search_result' => array(
            'type' => 'hidden',
            'label' => 'above',
            'settings' => array(),
            'weight' => 0,
          ),
        ),
        'required' => 0,
        'description' => '',
        'default_value' => NULL,
        'field_name' => 'field_web_media_width',
        'entity_type' => 'node',
        'bundle' => 'audio_video',
        'deleted' => '0',
      ),
    ),
  ),
  'fieldgroups' => array(
    'group_thumbnail|node|auido_video|form' => (object) array(
      'id' => '1',
      'identifier' => 'group_thumbnail|node|auido_video|form',
      'group_name' => 'group_thumbnail',
      'entity_type' => 'node',
      'bundle' => 'audio_video',
      'mode' => 'form',
      'parent_name' => '',
      'table' => 'field_group',
      'type' => 'Normal',
      'export_type' => 1,
      'label' => 'Thumbnail',
      'weight' => '13',
      'children' => array(
        0 => 'field_image_url',
        1 => 'field_image_width',
        2 => 'field_image_height',
      ),
      'format_type' => 'fieldset',
      'format_settings' => array(
        'formatter' => 'collapsible',
        'instance_settings' => array(
          'description' => '',
          'classes' => '',
          'required_fields' => 1,
        ),
      ),
    ),
    'group_web_media|node|auido_video|form' => (object) array(
      'id' => '2',
      'identifier' => 'group_web_media|node|auido_video|form',
      'group_name' => 'group_web_media',
      'entity_type' => 'node',
      'bundle' => 'audio_video',
      'mode' => 'form',
      'parent_name' => '',
      'table' => 'field_group',
      'type' => 'Normal',
      'export_type' => 1,
      'label' => 'Web Quality',
      'weight' => '15',
      'children' => array(
        0 => 'field_web_media_url',
        1 => 'field_web_media_bitrate',
        2 => 'field_web_media_filesize',
        3 => 'field_web_media_width',
        4 => 'field_web_media_height',
      ),
      'format_type' => 'fieldset',
      'format_settings' => array(
        'formatter' => 'collapsible',
        'instance_settings' => array(
          'description' => '',
          'classes' => 'group-web-media field-group-fieldset',
          'required_fields' => 1,
        ),
      ),
    ),
    'group_audio_transcript|node|auido_video|form' => (object) array(
      'id' => '3',
      'identifier' => 'group_audio_transcript|node|auido_video|form',
      'group_name' => 'group_audio_transcript',
      'entity_type' => 'node',
      'bundle' => 'audio_video',
      'mode' => 'form',
      'parent_name' => '',
      'table' => 'field_group',
      'type' => 'Normal',
      'export_type' => 1,
      'label' => 'Audio Transcript',
      'weight' => '19',
      'children' => array(
        0 => 'field_audio_transcript_media_url',
        1 => 'field_audio_transcript_media_bit',
        2 => 'field_audio_transcript_media_fil',
      ),
      'format_type' => 'fieldset',
      'format_settings' => array(
        'formatter' => 'collapsible',
        'instance_settings' => array(
          'description' => '',
          'classes' => 'group-audio-transcript field-group-fieldset',
          'required_fields' => 1,
        ),
      ),
    ),
    'group_mobile_media|node|audio_video|form' => (object) array(
      'id' => '4',
      'identifier' => 'group_mobile_media|node|audio_video|form',
      'group_name' => 'group_mobile_media',
      'entity_type' => 'node',
      'bundle' => 'audio_video',
      'mode' => 'form',
      'parent_name' => '',
      'table' => 'field_group',
      'type' => 'Normal',
      'export_type' => 1,
      'label' => 'Mobile Quality',
      'weight' => '14',
      'children' => array(
        0 => 'field_mobile_media_url',
        1 => 'field_mobile_media_bitrate',
        2 => 'field_mobile_media_filesize',
        3 => 'field_mobile_media_width',
        4 => 'field_mobile_media_height',
      ),
      'format_type' => 'fieldset',
      'format_settings' => array(
        'formatter' => 'collapsible',
        'instance_settings' => array(
          'description' => '',
          'classes' => 'group-mobile-media field-group-fieldset',
          'required_fields' => 1,
        ),
      ),
    ),
    'group_high_quality_media|node|audio_video|form' => (object) array(
      'id' => '5',
      'identifier' => 'group_high_quality_media|node|audio_video|form',
      'group_name' => 'group_high_quality_media',
      'entity_type' => 'node',
      'bundle' => 'audio_video',
      'mode' => 'form',
      'parent_name' => '',
      'table' => 'field_group',
      'type' => 'Normal',
      'export_type' => 1,
      'label' => 'High Quality',
      'weight' => '16',
      'children' => array(
        0 => 'field_high_quality_media_url',
        1 => 'field_high_quality_media_bitrate',
        2 => 'field_high_quality_media_filesiz',
        3 => 'field_high_quality_media_width',
        4 => 'field_high_quality_media_height',
      ),
      'format_type' => 'fieldset',
      'format_settings' => array(
        'formatter' => 'collapsible',
        'instance_settings' => array(
          'description' => '',
          'classes' => 'group-high-quality-media field-group-fieldset',
          'required_fields' => 1,
        ),
      ),
    ),
    'group_source_file|node|audio_video|form' => (object) array(
      'id' => '6',
      'identifier' => 'group_source_file|node|audio_video|form',
      'group_name' => 'group_source_file',
      'entity_type' => 'node',
      'bundle' => 'audio_video',
      'mode' => 'form',
      'parent_name' => '',
      'table' => 'field_group',
      'type' => 'Normal',
      'export_type' => 1,
      'label' => 'Source File',
      'weight' => '18',
      'children' => array(
        0 => 'field_source_media_url',
        1 => 'field_source_media_bitrate',
        2 => 'field_source_media_filesize',
        3 => 'field_source_media_width',
        4 => 'field_source_media_height',
      ),
      'format_type' => 'fieldset',
      'format_settings' => array(
        'formatter' => 'collapsible',
        'instance_settings' => array(
          'description' => '',
          'classes' => 'group-source-file field-group-fieldset',
          'required_fields' => 1,
        ),
      ),
    ),
  ),
);

======================================================== taxonomy definitions:
$data = array(
  'bundles' => array(
    'media_type' => (object) array(
      'vid' => '3',
      'name' => 'Media Type',
      'machine_name' => 'media_type',
      'description' => '',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
      'language' => 'und',
      'i18n_mode' => '1',
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
      'bc_entity_type' => 'taxonomy_term',
    ),
    'network' => (object) array(
      'vid' => '6',
      'name' => 'Network',
      'machine_name' => 'network',
      'description' => 'BBG Entity and Grantee Networks',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
      'language' => 'und',
      'i18n_mode' => '1',
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
      'bc_entity_type' => 'taxonomy_term',
    ),
  ),
  'fields' => array(
    'field_remap_name' => array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'max_length' => '32',
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_remap_name' => array(
                'value' => 'field_remap_name_value',
                'format' => 'field_remap_name_format',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_remap_name' => array(
                'value' => 'field_remap_name_value',
                'format' => 'field_remap_name_format',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'format' => array(
          'table' => 'filter_format',
          'columns' => array(
            'format' => 'format',
          ),
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'field_name' => 'field_remap_name',
      'type' => 'text',
      'module' => 'text',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'columns' => array(
        'value' => array(
          'type' => 'varchar',
          'length' => '32',
          'not null' => FALSE,
        ),
        'format' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ),
      ),
      'bundles' => array(
        'taxonomy_term' => array(
          0 => 'network',
        ),
      ),
    ),
  ),
  'instances' => array(
    'field_remap_name' => array(
      0 => array(
        'label' => 'remap_name',
        'widget' => array(
          'weight' => '3',
          'type' => 'text_textfield',
          'module' => 'text',
          'active' => 1,
          'settings' => array(
            'size' => '32',
          ),
        ),
        'settings' => array(
          'text_processing' => '0',
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'above',
            'type' => 'text_plain',
            'weight' => '0',
            'settings' => array(),
            'module' => 'text',
          ),
        ),
        'required' => 1,
        'description' => 'Alternate name for Affiliate Network shown and indexed for users of BBGDirect web application. ',
        'default_value' => NULL,
        'field_name' => 'field_remap_name',
        'entity_type' => 'taxonomy_term',
        'bundle' => 'network',
        'deleted' => '0',
      ),
    ),
  ),
);

========================================================================== additional taxonomy

$data = array(
  'bundles' => array(
    'category' => (object) array(
      'vid' => '2',
      'name' => 'Category',
      'machine_name' => 'category',
      'description' => '',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
      'language' => 'und',
      'i18n_mode' => '4',
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
      'bc_entity_type' => 'taxonomy_term',
    ),
    'genre' => (object) array(
      'vid' => '7',
      'name' => 'Genre',
      'machine_name' => 'genre',
      'description' => 'Type of stream',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
      'language' => 'und',
      'i18n_mode' => '0',
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
      'bc_entity_type' => 'taxonomy_term',
    ),
    'programs' => (object) array(
      'vid' => '4',
      'name' => 'Programs',
      'machine_name' => 'programs',
      'description' => 'List of reoccuring broadcast programs',
      'hierarchy' => '1',
      'module' => 'taxonomy',
      'weight' => '0',
      'language' => 'und',
      'i18n_mode' => '4',
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
      'bc_entity_type' => 'taxonomy_term',
    ),
    'region' => (object) array(
      'vid' => '5',
      'name' => 'Region',
      'machine_name' => 'region',
      'description' => 'Geographic region for a content item',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
      'language' => 'und',
      'i18n_mode' => '4',
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
      'bc_entity_type' => 'taxonomy_term',
    ),
    'tags' => (object) array(
      'vid' => '1',
      'name' => 'Tags',
      'machine_name' => 'tags',
      'description' => 'Use tags to group articles on similar topics into categories.',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
      'language' => 'und',
      'i18n_mode' => '1',
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
      'bc_entity_type' => 'taxonomy_term',
    ),
  ),
  'fields' => array(
    'field_image' => array(
      'indexes' => array(
        'fid' => array(
          0 => 'fid',
        ),
      ),
      'settings' => array(
        'uri_scheme' => 's3',
        'default_image' => 0,
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_image' => array(
                'fid' => 'field_image_fid',
                'alt' => 'field_image_alt',
                'title' => 'field_image_title',
                'width' => 'field_image_width',
                'height' => 'field_image_height',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_image' => array(
                'fid' => 'field_image_fid',
                'alt' => 'field_image_alt',
                'title' => 'field_image_title',
                'width' => 'field_image_width',
                'height' => 'field_image_height',
              ),
            ),
          ),
        ),
      ),
      'entity_types' => array(),
      'translatable' => '0',
      'foreign keys' => array(
        'fid' => array(
          'table' => 'file_managed',
          'columns' => array(
            'fid' => 'fid',
          ),
        ),
      ),
      'field_name' => 'field_image',
      'type' => 'image',
      'module' => 'image',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'columns' => array(
        'fid' => array(
          'description' => 'The {file_managed}.fid being referenced in this field.',
          'type' => 'int',
          'not null' => FALSE,
          'unsigned' => TRUE,
        ),
        'alt' => array(
          'description' => 'Alternative image text, for the image\'s \'alt\' attribute.',
          'type' => 'varchar',
          'length' => 512,
          'not null' => FALSE,
        ),
        'title' => array(
          'description' => 'Image title text, for the image\'s \'title\' attribute.',
          'type' => 'varchar',
          'length' => 1024,
          'not null' => FALSE,
        ),
        'width' => array(
          'description' => 'The width of the image in pixels.',
          'type' => 'int',
          'unsigned' => TRUE,
        ),
        'height' => array(
          'description' => 'The height of the image in pixels.',
          'type' => 'int',
          'unsigned' => TRUE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'article',
          1 => 'page',
          2 => 'marketing_post',
          3 => 'language_message',
        ),
        'taxonomy_term' => array(
          0 => 'programs',
        ),
      ),
    ),
  ),
  'instances' => array(
    'field_image' => array(
      0 => array(
        'label' => 'Image',
        'widget' => array(
          'weight' => '32',
          'type' => 'image_image',
          'module' => 'image',
          'active' => 1,
          'settings' => array(
            'progress_indicator' => 'throbber',
            'preview_image_style' => 'thumbnail',
          ),
        ),
        'settings' => array(
          'file_directory' => '',
          'file_extensions' => 'png gif jpg jpeg',
          'max_filesize' => '',
          'max_resolution' => '',
          'min_resolution' => '',
          'alt_field' => 1,
          'title_field' => 1,
          'default_image' => 0,
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'hidden',
            'type' => 'image',
            'weight' => '1',
            'settings' => array(
              'image_style' => 'large',
              'image_link' => '',
            ),
            'module' => 'image',
          ),
        ),
        'required' => 0,
        'description' => '',
        'field_name' => 'field_image',
        'entity_type' => 'taxonomy_term',
        'bundle' => 'programs',
        'deleted' => '0',
      ),
    ),
  ),
);
=============================================================================================
network taxonomy terms:
 add terms as follows
VOA Central
Marti
MBN
RFA
VOA-Central
RFE/RL
-Настоящее время
VOA
 for each of these terms a remap_name field exist.
 this field must be set as follows:
VOA Central: voa-central
Marti:       ocb
MBN
RFA
VOA-Central
RFE/RL:      rferl
-Настоящее время
VOA


